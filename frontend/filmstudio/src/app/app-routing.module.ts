import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import { LoginComponent } from './security/login/login.component';
import { AdminGuard } from './security/authorization/admin.guard';
import { UserGuard } from './security/authorization/user.guard';
import { MovieDetailsComponent } from './movies/movie-details/movie-details.component';
import { AddMovieComponent } from './movies/add-movie/add-movie.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { AddDirectorComponent } from './movies/add-director/add-director.component';
import { MovieDetailsResolverService } from './movies/movie-details-resolver.service';
import { MovieGenresResolverService } from './movies/movie-genres-resolver.service';
import { RegisterComponent } from './security/register/register.component';
import { MoviesResolverService } from './movies/movies-resolver.service';

const routes: Routes = [
    {path: '', redirectTo: 'movies', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'director', canActivate: [AdminGuard], component: AddDirectorComponent},
    {
      path: 'movies',
      canActivate: [UserGuard],
      resolve: {
        genres: MovieGenresResolverService,
        movies: MoviesResolverService
      },
      component: MoviesListComponent},
    {path: 'users', canActivate: [UserGuard], component: UsersListComponent},
    {path: 'movies/add', canActivate: [AdminGuard], component: AddMovieComponent},
    {
      path: 'movies/:id',
      canActivate: [UserGuard],
      resolve: {
        movie: MovieDetailsResolverService,
        genres: MovieGenresResolverService
      },
      component: MovieDetailsComponent
    },
];

@NgModule({
    imports: [
      RouterModule.forRoot(routes),
      CommonModule
    ],
    exports: [RouterModule],
    declarations: [],
    providers: [
      AdminGuard,
      UserGuard,
      MovieDetailsResolverService,
      MoviesResolverService,
      MovieGenresResolverService]
  })

export class AppRoutingModule {
}


