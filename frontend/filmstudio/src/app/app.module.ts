import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http/';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './nav/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './security/login/login.component';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import { MovieDetailsComponent } from './movies/movie-details/movie-details.component';
import { MovieService } from './movies/movie.service';
import { AddMovieComponent } from './movies/add-movie/add-movie.component';
import { DirectorService } from './movies/director.service';
import { AuthenticationService } from './security/authentication/authentication.service';
import { UserService } from './security/user.service';
import { UsersListComponent } from './users/users-list/users-list.component';
import { MovieCommentsComponent } from './movies/movie-comments/movie-comments.component';
import { UserObservationService } from './users/user-observation.service';
import { AddDirectorComponent } from './movies/add-director/add-director.component';
import { MovieScoreComponent } from './movies/movie-score/movie-score.component';
import { RegisterComponent } from './security/register/register.component';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CookiesComponent } from './cookies/cookies.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    MoviesListComponent,
    MovieDetailsComponent,
    AddMovieComponent,
    UsersListComponent,
    MovieCommentsComponent,
    AddDirectorComponent,
    MovieScoreComponent,
    RegisterComponent,
    CookiesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    MovieService,
    DirectorService,
    AuthenticationService,
    UserService,
    UserObservationService,
    CookieService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
