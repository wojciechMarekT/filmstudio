import {Component, OnChanges, OnInit} from '@angular/core';
import {UserObservationService} from '../user-observation.service';
import {User} from '../../security/user';
import {UserService} from '../../security/user.service';

@Component({
  selector: 'fs-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users: User[] = [];
  currentUser: User;
  observedUsers: User[] = [];
  errorMessage: string;

  constructor(private observationService: UserObservationService,
              private userService: UserService) { }

  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
    this.getObservedUsers();
  }

  getUsers() {
    this.observationService.getUsers().subscribe(
      data => this.users = data,
      error => this.errorMessage = 'Failed to download the list of users.');
  }

  checkForObservedUser(id: number): boolean {
   const observedUser = this.observedUsers.find(x => x.Id === id);
   if (observedUser) {
     return true;
   } else {
     return false;
   }
  }

  observeUser(userIdToObserve: number) {
    this.observationService.observeUser(this.userService.getCurrentUser().Id, userIdToObserve).subscribe(
      data => this.getObservedUsers(),
    error => this.errorMessage = 'Failed to observe user.');
  }

  getObservedUsers() {
    this.observationService.getObservedUsers().subscribe(
      data => {
        this.observedUsers = data;
        this.getUsers();
      },
      error => this.errorMessage = 'Failed to download the list of observed users.');
  }

  makeAdmin(userId: number) {
    this.observationService.makeAdmin(userId).subscribe(
      data => this.getUsers() );
  }

}
