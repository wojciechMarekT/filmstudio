import { TestBed, inject } from '@angular/core/testing';

import { UserObservationService } from './user-observation.service';

describe('UserObservationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserObservationService]
    });
  });

  it('should be created', inject([UserObservationService], (service: UserObservationService) => {
    expect(service).toBeTruthy();
  }));
});
