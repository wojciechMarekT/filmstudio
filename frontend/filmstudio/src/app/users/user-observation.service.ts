import { Injectable } from '@angular/core';
import {Movie} from '../movies/movie';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import {User} from '../security/user';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../security/user.service';

@Injectable()
export class UserObservationService {

  constructor(private httpClient: HttpClient,
              private userService: UserService) { }

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(environment.apiAddress + 'users/');
  }

  getObservedUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(environment.apiAddress + 'users/observed?userId=' + this.userService.getCurrentUser().Id);
  }

  observeUser(observingUserID: number, observedUserId: number): Observable<any> {
   return this.httpClient.put(environment.apiAddress + '/users/observe?observing=' + observingUserID + '&observed=' + observedUserId, null);
  }

  makeAdmin(userId: number) {
    return this.httpClient.put(environment.apiAddress + '/users/' + userId + '/setadmin', null);
  }
}
