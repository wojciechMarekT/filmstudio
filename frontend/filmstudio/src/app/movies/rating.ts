import { User } from '../security/user';
import { Movie } from './movie';

export class Rating {
    User: User;
    Movie: Movie;
    Value: number;
}
