import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve } from '@angular/router/';
import { Movie } from './movie';
import { MovieService } from './movie.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MovieGenre } from './movie-genre';

@Injectable()
export class MovieGenresResolverService implements Resolve<MovieGenre[]> {

  constructor(private movieService: MovieService,
              private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): MovieGenre[] | Observable<MovieGenre[]> {
    return this.movieService.getMovieGenres();
  }

}
