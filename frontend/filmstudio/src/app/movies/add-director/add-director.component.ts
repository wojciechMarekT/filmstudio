import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router/';
import { HttpErrorResponse } from '@angular/common/http';

import { DirectorService } from '../director.service';
import { Director } from '../director';

@Component({
  selector: 'fs-add-director',
  templateUrl: './add-director.component.html',
  styleUrls: ['./add-director.component.css']
})
export class AddDirectorComponent implements OnInit {

  constructor(private directorService: DirectorService,
              private router: Router) { }

  director = new Director();

  ngOnInit() {
  }

  addNewDirector() {
    this.directorService.postDirector(this.director).subscribe(resp => {
      const id = resp.Id;
      this.director = new Director();
      this.router.navigateByUrl('movies');
    },
    err => {const errorResponse = err as HttpErrorResponse;
          console.log(errorResponse.headers); });
  }

}
