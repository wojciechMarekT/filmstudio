import {Movie} from './movie';
import {User} from '../security/user';

export class MovieScore {
  Id: number;
  Movie: Movie;
  User: User;
  Score: number;
}
