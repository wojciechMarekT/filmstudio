import { User } from '../security/user';
import { Movie } from './movie';

export class Comment {
  Id: number;
  User: User;
  Movie: Movie;
  UserComment: string;
}
