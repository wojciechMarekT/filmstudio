import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router/';

import { MovieService } from '../movie.service';
import { Movie } from '../movie';
import { MovieGenre } from '../movie-genre';
import { Director } from '../director';
import { DirectorService } from '../director.service';
import { HttpErrorResponse } from '@angular/common/http/src/response';

@Component({
  selector: 'fs-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  movieGenres: MovieGenre[] = [];
  directors: Director[] = [];
  movie = new Movie();

  constructor(private movieService: MovieService,
              private directorService: DirectorService,
              private router: Router) { }

  ngOnInit() {
    this.getGenres();
    this.getDirectors();
  }

  getGenres() {
    this.movieService.getMovieGenres().subscribe(genres => this.movieGenres = genres);
  }

  getDirectors() {
    this.directorService.getDirectors().subscribe(directors => this.directors = directors);
  }

  addNewMovie() {
    this.movieService.postMovie(this.movie).subscribe(resp => {
      const id = resp.Id;
      this.movie = new Movie();
      this.router.navigateByUrl('movies/' + id);
    },
    err => {const errorResponse = err as HttpErrorResponse;
          console.log(errorResponse.headers); });
  }
}
