import { TestBed, inject } from '@angular/core/testing';

import { MovieGenresResolverService } from './movie-genres-resolver.service';

describe('MovieGenresResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieGenresResolverService]
    });
  });

  it('should be created', inject([MovieGenresResolverService], (service: MovieGenresResolverService) => {
    expect(service).toBeTruthy();
  }));
});
