import { Component, OnInit } from '@angular/core';
import {MovieScore} from '../movieScore';
import {MovieService} from '../movie.service';
import {UserService} from '../../security/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'fs-movie-score',
  templateUrl: './movie-score.component.html',
  styleUrls: ['./movie-score.component.css']
})
export class MovieScoreComponent implements OnInit {

  movieId: number;
  userId: number;
  userScore: number;
  movieScores: MovieScore[];
  errorMessage: string;
  newUserScore: number;

  constructor(private movieService: MovieService,
              private userservice: UserService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.movieId = +this.route.snapshot.paramMap.get('id');
    this.userId = this.userservice.getCurrentUser().Id;
    this.movieScores = [];
    this.userScore = null;
    this.getMovieScores();
  }

  getMovieScores() {
    this.movieService.getMovieScores(this.userId, this.movieId )
      .subscribe(data => {
          if (data.length !== 0) {
            this.movieScores = data;
          }
          this.setUserScore();
        },
        error => this.errorMessage = 'Failed to download movie scores.');
  }
  setUserScore() {
   const score = this.movieScores.find(x => x.User.Id === this.userId);
   if (score === undefined) {
     this.userScore = 0;
   }else {
     this.userScore = score.Score;
   }
  }

  setScoreToMovie() {
    this.movieService.setMovieScore(this.movieId, this.userId, this.newUserScore).subscribe(data => this.getMovieScores()
    );
  }
}
