import { Component, OnInit } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';
import { Router, ActivatedRoute } from '@angular/router/';
import { MovieGenre } from '../movie-genre';
import {Director} from '../director';

@Component({
  selector: 'fs-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  movie: Movie;
  movieGenres: MovieGenre[] = [];
  id: number;

  constructor(private movieService: MovieService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.movieGenres = this.route.snapshot.data['genres'];
    this.movie = this.route.snapshot.data['movie'];
    if (!this.movie) {
      this.router.navigateByUrl('/movies');
    }
    this.inputGenres();
  }

  inputGenres() {
    this.movie.Genre = this.movieGenres.find(x => x.Value === this.movie.Genre).Name;
  }

}
