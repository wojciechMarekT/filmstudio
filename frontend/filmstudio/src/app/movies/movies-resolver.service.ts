import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve } from '@angular/router/';
import { Movie } from './movie';
import { MovieService } from './movie.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class MoviesResolverService implements Resolve<Movie[]> {

  constructor(private movieService: MovieService,
              private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Movie[] | Observable<Movie[]> {
    return this.movieService.getMovies();
  }
}
