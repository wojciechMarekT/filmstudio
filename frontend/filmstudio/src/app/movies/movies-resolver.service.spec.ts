import { TestBed, inject } from '@angular/core/testing';

import { MoviesResolverService } from './movies-resolver.service';

describe('MoviesResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoviesResolverService]
    });
  });

  it('should be created', inject([MoviesResolverService], (service: MoviesResolverService) => {
    expect(service).toBeTruthy();
  }));
});
