import { Director } from './director';

export class Movie {
    Id: number;
    Title: string;
    Director: Director;
    ReleaseYear: number;
    Description: string;
    AverageScore: number;
    Genre: string;
}
