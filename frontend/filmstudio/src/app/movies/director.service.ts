import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http/';
import { Observable } from 'rxjs/Observable';
import { Director } from './director';
import { environment } from '../../environments/environment';

@Injectable()
export class DirectorService {

  constructor(private httpClient: HttpClient) { }

  getDirectors(): Observable<Director[]> {
    return this.httpClient.get<Director[]>(environment.apiAddress + '/directors');
  }

  postDirector(director: Director): Observable<Director> {
    return this.httpClient.post<Director>(environment.apiAddress + '/directors', director);
  }

}
