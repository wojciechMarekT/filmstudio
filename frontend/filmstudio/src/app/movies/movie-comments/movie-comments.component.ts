import {Component, OnInit, OnChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MovieService} from '../movie.service';
import {Comment} from '../comment';
import {UserService} from '../../security/user.service';
import {User} from '../../security/user';
import {Movie} from '../movie';

@Component({
  selector: 'fs-movie-comments',
  templateUrl: './movie-comments.component.html',
  styleUrls: ['./movie-comments.component.css']
})
export class MovieCommentsComponent implements OnInit, OnChanges {

  id: number;
  comments: Comment[];
  newComment: Comment;

  constructor(private movieService: MovieService,
              private route: ActivatedRoute,
              private userService: UserService) {
  }

  ngOnInit() {
    this.comments = [];
    this.newComment = new Comment();
    this.newComment.User = new User(0, null, false, false);
    this.newComment.Movie = new Movie();

    this.id = +this.route.snapshot.paramMap.get('id');
    this.getComments();
  }

  ngOnChanges(): void {
    this.getComments();
  }

  getComments() {
    this.movieService.getMovieComments(this.id)
      .subscribe(data => this.comments = data);
  }

  addComment() {
    this.newComment.User = this.userService.getCurrentUser();
    this.movieService.getMovie(this.id).subscribe(data => {
      this.newComment.Movie = data;
      this.movieService.addComment(this.newComment).subscribe(() => this.getComments());
    });
  }
}
