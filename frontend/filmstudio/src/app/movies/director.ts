export class Director {
    Id: number;
    Name: string;
    Surname: string;
    BirthDate: Date;
}
