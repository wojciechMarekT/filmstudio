import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Comment} from './comment';
import {Movie} from './movie';
import { environment } from '../../environments/environment';
import { MovieGenre } from './movie-genre';
import {MovieScore} from './movieScore';
import {User} from '../security/user';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/finally';

@Injectable()
export class MovieService {
  movieGenres: MovieGenre[] = [];
  constructor(private httpClient: HttpClient) { }

  getMovieGenres() {
    return this.httpClient.get<MovieGenre[]>(environment.apiAddress + 'movies/genre');
  }

  getMovies(): Observable<Movie[]> {
    return this.httpClient.get<Movie[]>(environment.apiAddress + 'movies/');
  }

  getMovie(id: number): Observable<Movie> {
    return this.httpClient.get<Movie>(environment.apiAddress + 'movies/' + id);
  }

  postMovie(movie: Movie): Observable<Movie> {
    return this.httpClient.post<Movie>(environment.apiAddress + 'movies/add', movie);
  }

  getMovieComments(id: number): Observable<Comment[]> {
    return this.httpClient.get<Comment[]>(environment.apiAddress + 'moviecomments/' + id);
  }

  getMovieScores(userId: number, movieId: number): Observable<MovieScore[]> {
    return this.httpClient.get<MovieScore[]>(environment.apiAddress + 'MovieScores?movieID=' + movieId + '&userId=' + userId);
  }

  setMovieScore(movieId: number, userId: number, score: number): Observable<MovieScore> {
    const movieScore = new MovieScore();
    movieScore.Movie = new Movie();
    movieScore.Movie.Id = movieId;
    movieScore.Score = score;
    movieScore.User = new User(userId, null, false, false );

    return this.httpClient.post<MovieScore>(environment.apiAddress + 'movieScores', movieScore  );
  }
  addComment(newComment: Comment): Observable<Comment> {
    return this.httpClient.post<Comment>(environment.apiAddress + 'moviecomments/', newComment);
  }
}
