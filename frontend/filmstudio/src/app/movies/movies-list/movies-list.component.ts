import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { Movie } from '../movie';
import { MovieGenre } from '../movie-genre';
import { Router, ActivatedRoute } from '@angular/router/';

@Component({
  selector: 'fs-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  movieGenres: MovieGenre[] = [];
  movies: Movie[] = [];
  errorMessage: string;

  constructor(private movieService: MovieService,
    private router: Router,
    private route: ActivatedRoute) { }

    ngOnInit() {
      this.movieGenres = this.route.snapshot.data['genres'];
      this.movies = this.route.snapshot.data['movies'];
      this.movies.forEach((item) => item.Genre = this.movieGenres.find(x => x.Value === item.Genre).Name);
    }
  }
