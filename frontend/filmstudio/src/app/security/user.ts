export class User {
  constructor( public Id: number,
  public Login: string,
  public IsLoggedIn: boolean,
  public IsAdministrator: boolean) {}
}
