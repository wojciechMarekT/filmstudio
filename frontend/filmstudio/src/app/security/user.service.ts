import { Injectable } from '@angular/core';
import { User } from './user';
import {CookieService} from 'angular2-cookie/core';

@Injectable()
export class UserService {

  constructor(private cookieService: CookieService) { }

  getCurrentUser(): User {
    const currentUser =  this.cookieService.getObject('user');
    if (currentUser) {
      return currentUser as User;
    }
    return null;
  }

  setCurrentUser(user: User) {
    this.cookieService.putObject('user', user);
  }

  removeUser() {
    this.cookieService.remove('user');
  }

}
