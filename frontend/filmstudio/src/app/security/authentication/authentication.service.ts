import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {Credentials} from '../credentials';
import {Observable} from 'rxjs/Observable';
import {User} from '../user';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient,
              private userService: UserService,
              private router: Router) { }

  login(credentials: Credentials): Observable<User> {
    return this.http
      .post<any>(environment.apiAddress + 'authentication', credentials)
      .pipe(
        map(response => {
          const user = new User(response.Id, response.Login, response.IsLoggedIn, response.IsAdministrator);
          return user;
        })
      );
  }
  register(credentials: Credentials): Observable<User> {
    return this.http
      .post<any>(environment.apiAddress + 'users/register', credentials)
      .pipe(
        map(response => {
          let user = null;
          if (response != null) {
           user = new User(response.Id, response.Login, response.IsLoggedIn, response.IsAdministrator);
          }
          return user;
        })
      );
  }

  logout() {
    this.userService.removeUser();
    this.router.navigateByUrl('/login');
  }
}
