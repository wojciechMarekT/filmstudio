import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication/authentication.service';
import {Credentials} from '../credentials';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'fs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: Credentials = new Credentials();
  errorMessage: string;

  constructor(private authService: AuthenticationService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.credentials)
      .subscribe(user => {
          if (user.IsLoggedIn) {
            this.userService.setCurrentUser(user);
            this.router.navigateByUrl('/movies');
          } else {
            this.errorMessage = 'Incorrect username or password.';
          }
          this.credentials = new Credentials();
        },
        error => this.errorMessage = 'Server is not answearing');
  }
}
