import { Component, OnInit } from '@angular/core';
import {Credentials} from '../credentials';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication/authentication.service';

@Component({
  selector: 'fs-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  credentials: Credentials = new Credentials();
  errorMessage: string;

  constructor(private authService: AuthenticationService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }


  register() {
    this.authService.register(this.credentials)
      .subscribe(user => {
          if (user != null) {
           this.router.navigateByUrl('/login');
          } else {
            this.errorMessage = 'Username taken.';
          }
          this.credentials = new Credentials();
        },
        error => this.errorMessage = 'Server is not answearing');
  }
}
