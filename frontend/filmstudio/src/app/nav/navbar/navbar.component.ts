import { Component, OnInit } from '@angular/core';

import {AuthenticationService} from '../../security/authentication/authentication.service';
import { UserService } from '../../security/user.service';
import { User } from '../../security/user';

@Component({
  selector: 'fs-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: User;

  constructor(private authService: AuthenticationService,
              public userService: UserService) { }

  ngOnInit() {
    this.user = this.userService.getCurrentUser();
  }

  logout() {
    this.authService.logout();
  }
}
