import { Component, OnInit, Injectable } from '@angular/core';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'fs-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.css']
})

@Injectable()
export class CookiesComponent implements OnInit {

  constructor(private cookieService: CookieService) { }

  ngOnInit() {
  }

  setCookieAgreement() {
    this.cookieService.put('agreement', 'yes');
  }

  getCookieAgreement() {
    return this.cookieService.get('agreement');
  }
}
