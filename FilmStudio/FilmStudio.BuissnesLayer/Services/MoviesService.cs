﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.BuissnesLayer.Services
{
    public class MoviesService : IMoviesService
    {
        private readonly IMoviesRepository _moviesRepository;
        private readonly IMovieCommentRepository _movieCommentRepository;
        private readonly IMovieScoreRepository _movieScoreRepository;
        private readonly IMapper _mapper;

        public MoviesService(IMoviesRepository moviesRepository, IMovieCommentRepository movieCommentRepository, IMovieScoreRepository movieScoreRepository, IMapper mapper)
        {
            _moviesRepository = moviesRepository;
            _movieCommentRepository = movieCommentRepository;
            _movieScoreRepository = movieScoreRepository;
            _mapper = mapper;
        }

        public int AddMovie(MovieDto movieDto)
        {
            if (movieDto == null)
            {
                return 0;
            }
            var newId = _moviesRepository.AddMovie(_mapper.Map<MovieDto, Movie>(movieDto));
            return newId;
        }

        public List<MovieDto> GetAllMovies()
        {
            var movieList = _moviesRepository.GetAllMovies()
                .Select(movie => _mapper.Map<Movie, MovieDto>(movie))
                .ToList();

            movieList.ForEach(m => m.AverageScore = _moviesRepository.GetMovieScore(m.Id));

            return movieList;
        }

        public MovieDto GetMovieById(int movieId)
        {
           var movie = _moviesRepository.GetMovieById(movieId);
            var movieDto = _mapper.Map<Movie, MovieDto>(movie);
            movieDto.AverageScore = _moviesRepository.GetMovieScore(movieDto.Id);
            return movieDto;
        }

        public int AddComment(CommentDto commentDto)
        {
            if (commentDto == null)
            {
                return 0;
            }

            var addedComment = _movieCommentRepository.AddComment(_mapper.Map<CommentDto, Comment>(commentDto));
            return addedComment.Id;
        }

        public List<CommentDto> GetComments(int movieId)
        {
            return _movieCommentRepository.GetComments(movieId)
                .Select(x => _mapper.Map<Comment, CommentDto>(x))
                .ToList();
        }

        public int AddScore(MovieScoreDto movieScoreDto)
        {
            if (movieScoreDto == null)
            {
                return 0;
            }

            var score = _mapper.Map<MovieScoreDto, MovieScore>(movieScoreDto);
            var addedScore = _movieScoreRepository.AddScore(score);
            return addedScore.Id;
        }

        public List<MovieScoreDto> GetScores(int movieId, int userId)
        {
            var scores = _movieScoreRepository.GetScore(movieId, userId);
            return scores.Select(x => _mapper.Map<MovieScore, MovieScoreDto>(x)).ToList();
        }

        public PageResultDto<MovieDto> GetPagedMovies(int pageSize, int pageNo, string orderBy)
        {
            var page = _moviesRepository.GetPagedMovies(pageSize, pageNo, orderBy);

            var pageDto = _mapper.Map<PageResults<Movie>, PageResultDto<MovieDto>> (page);
            return pageDto;
        }
    }
}
