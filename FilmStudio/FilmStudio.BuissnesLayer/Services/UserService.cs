﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.BuissnesLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public UserDto RegisterUSer(UserDto userDto)
        {
            var user = _mapper.Map<UserDto, User>(userDto);
            var result = _userRepository.RegisterUser(user);

            return _mapper.Map<User, UserDto>(result);
        }

        public UserDto GetUserByLogin(string login)
        {
           var user = _userRepository.GetUserByLogin(login);

            return _mapper.Map<User, UserDto>(user);
        }

        public List<UserDto> GetAllUsers()
        {
            var users = _userRepository.GetAllUsers()
                .Select(user => _mapper.Map<User, UserDto>(user))
                .ToList();

            return users;
        }

        public void SetAdminPrivilidges(int userId)
        {
            _userRepository.SetAdministratorPriviliges(userId);
        }

        public void ObserveUSer(int observing, int observed)
        {
            _userRepository.ObserveUser(observing, observed);
        }

        public List<UserDto> GetObservedUsers(int userId)
        {
            var users = _userRepository.GetObservedUsers(userId)
                .Select(user => _mapper.Map<User, UserDto>(user))
                .ToList();

            return users;
        }
    }
}
