﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.BuissnesLayer.Services.Interfaces
{
    public interface IUserService
    {
        UserDto RegisterUSer(UserDto userDto);
        UserDto GetUserByLogin(string login);
        void SetAdminPrivilidges(int userId);
        void ObserveUSer(int observing, int observed);
        List<UserDto> GetAllUsers();
        List<UserDto> GetObservedUsers(int userId);
    }
}