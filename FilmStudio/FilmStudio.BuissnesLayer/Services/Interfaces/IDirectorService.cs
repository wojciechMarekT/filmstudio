﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.BuissnesLayer.Services.Interfaces
{
    public interface IDirectorService
    {
        int AddDirector(DirectorDto directorDto);
        List<DirectorDto> GetAllDirectors();
    }
}