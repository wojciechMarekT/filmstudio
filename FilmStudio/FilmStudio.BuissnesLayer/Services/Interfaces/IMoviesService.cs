﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.BuissnesLayer.Services.Interfaces
{
    public interface IMoviesService
    {
        int AddMovie(MovieDto movieDto);
        List<MovieDto> GetAllMovies();
        MovieDto GetMovieById(int movieId);
        int AddComment(CommentDto commentDto);
        List<CommentDto> GetComments(int movieId);
        int AddScore(MovieScoreDto movieScoreDto);
        List<MovieScoreDto> GetScores(int movieId, int userId);
        PageResultDto<MovieDto> GetPagedMovies(int pageSize, int pageNo, string orderBy);
    }
}