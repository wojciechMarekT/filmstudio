﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.BuissnesLayer.Services
{
    public class DirectorService : IDirectorService
    {
        private readonly IDirectorRepository _directorRepository;
        private readonly IMapper _mapper;

        public DirectorService(IDirectorRepository directorRepository, IMapper mapper)
        {
            _directorRepository = directorRepository;
            _mapper = mapper;
        }

        public int AddDirector(DirectorDto directorDto)
        {
            var director = _mapper.Map<DirectorDto, Director>(directorDto);
            return _directorRepository.AddDirector(director);
        }

        public List<DirectorDto> GetAllDirectors()
        {
            var directorsList = _directorRepository.GetDiretors()
                .Select(director => _mapper.Map<Director, DirectorDto>(director))
                .ToList();

            return directorsList;
        }
    }
}
