﻿using AutoMapper;
using FilmStudio.BuissnesLayer.Extensions;
using FilmStudio.BuissnesLayer.Services;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.DataLayer.InjectComponents;
using Ninject.Modules;

namespace FilmStudio.BuissnesLayer.InjectComponents
{
    public class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel?.Load(new[] { new DataModule() });
            Bind<IUserService>().To<UserService>();
            Bind<IMoviesService>().To<MoviesService>();
            Bind<IDirectorService>().To<DirectorService>();
            Bind<IEnumExtension>().To<EnumExtension>();
            Bind<IMapper>().ToProvider<MapperProvider>();
        }
    }
}
