﻿using AutoMapper;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.DataLayer.Models;
using Ninject.Activation;

namespace FilmStudio.BuissnesLayer.InjectComponents
{
    internal class MapperProvider : Provider<IMapper>
    {
        protected override IMapper CreateInstance(IContext context)
        {
            var mapperConfiguration = new MapperConfiguration(config =>
            {
                config.CreateMap<User, UserDto>().MaxDepth(1);
                config.CreateMap<UserDto, User>()
                .ForMember(dest => dest.ObservingUsers, opt => opt.Ignore());


                config.CreateMap<DirectorDto, Director>();
                config.CreateMap<Director, DirectorDto>();

                config.CreateMap<Movie, MovieDto>()
                .ForMember(dest => dest.AverageScore, opt => opt.Ignore())
                .MaxDepth(1);
                config.CreateMap<MovieDto, Movie>().MaxDepth(1);


                config.CreateMap<MovieScore, MovieScoreDto>().MaxDepth(1);
                config.CreateMap<MovieScoreDto, MovieScore>().MaxDepth(1);

                config.CreateMap<Comment, CommentDto>().MaxDepth(1);
                config.CreateMap<CommentDto, Comment>().MaxDepth(1);
                config.CreateMap<PageResults<Movie>, PageResultDto<MovieDto>>().MaxDepth(1);
            });
            return new Mapper(mapperConfiguration);
        }
    }
}
