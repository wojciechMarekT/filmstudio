﻿namespace FilmStudio.BuissnesLayer.Models
{
    public class MovieScoreDto
    {
        public int Id { get; set; }
        public MovieDto  Movie { get; set; }
        public UserDto User { get; set; }
        public int Score { get; set; }
    }
}
