﻿namespace FilmStudio.BuissnesLayer.Models
{
    public class EnumValue
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
