﻿using System;

namespace FilmStudio.BuissnesLayer.Models
{
    public class DirectorDto
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
