﻿using System.Collections.Generic;

namespace FilmStudio.BuissnesLayer.Models
{
    public class PageResultDto<T>
    {
        public List<T> Collection { get; set; }
        public int Pages { get; set; }
    }
}
