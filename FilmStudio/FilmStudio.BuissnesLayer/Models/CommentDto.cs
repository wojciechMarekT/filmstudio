﻿namespace FilmStudio.BuissnesLayer.Models
{
    public class CommentDto
    {
        public int Id { get; set; }
        public UserDto User { get; set; }
        public MovieDto Movie { get; set; }
        public string UserComment { get; set; }
    }
}
