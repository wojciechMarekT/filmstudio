﻿using System.Collections.Generic;

namespace FilmStudio.BuissnesLayer.Models
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public List<UserDto> ObservedUsers { get; set; }
    }
}
