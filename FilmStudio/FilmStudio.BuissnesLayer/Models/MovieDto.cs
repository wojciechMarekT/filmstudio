﻿namespace FilmStudio.BuissnesLayer.Models
{
    public enum GenreDto { Thriller, Comedy, Horror, Action, SciFi, Adventure, Drama }

    public class MovieDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DirectorDto Director { get; set; }
        public int ReleaseYear { get; set; }
        public string Description { get; set; }
        public GenreDto Genre { get; set; }
        public double AverageScore { get; set; }
    }
}
