﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.BuissnesLayer.Extensions
{
    public interface IEnumExtension
    {
        List<EnumValue> GetValues<T>();
    }
}
