﻿using System;
using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.BuissnesLayer.Extensions
{


    public class EnumExtension : IEnumExtension
    {
        public List<EnumValue> GetValues<T>()
        {
            var valuesList = new List<EnumValue>();

            foreach (var item in Enum.GetValues(typeof(T)))
            {
                valuesList.Add(new EnumValue
                {
                    Name = Enum.GetName(typeof(T), item),
                    Value = (int)item
                });
            }

            return valuesList;
        }
    }
}
