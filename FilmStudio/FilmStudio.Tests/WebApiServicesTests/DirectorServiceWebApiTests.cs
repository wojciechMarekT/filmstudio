﻿using System;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FilmStudio.Tests.WebApiServicesTests
{
    [TestClass]
    public class DirectorServiceWebApiTests
    {
        [TestMethod]
        public void AddDirector_null_null()
        {
            //Arrange
            var directorServiceMock = new Mock<IDirectorService>();
            var service = new DirectorServiceWebApi(directorServiceMock.Object);
            //Act
            var response = service.AddDirector(null);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddDirector_dataWithoutDirectorName_null()
        {
            //Arrange
            var directorServiceMock = new Mock<IDirectorService>();
            var service = new DirectorServiceWebApi(directorServiceMock.Object);
            var director = new DirectorDto
            {
                Id = 1,
                Name = " ",
                Surname = "Nowak",
                BirthDate = new DateTime(2000, 10, 10)
            };
            //Act
            var response = service.AddDirector(director);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddDirector_dataWithoutDirectorSurname_null()
        {
            //Arrange
            var directorServiceMock = new Mock<IDirectorService>();
            var service = new DirectorServiceWebApi(directorServiceMock.Object);
            var director = new DirectorDto
            {
                Id = 1,
                Name = "Jan",
                Surname = null,
                BirthDate = new DateTime(2000, 10, 10)
            };
            //Act
            var response = service.AddDirector(director);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddDirector_dataWithBirthDateOutOfRange_null()
        {
            //Arrange
            var directorServiceMock = new Mock<IDirectorService>();
            var service = new DirectorServiceWebApi(directorServiceMock.Object);
            var director = new DirectorDto
            {
                Id = 1,
                Name = "Jan",
                Surname = "Nowak",
                BirthDate = new DateTime(1000, 10, 10)
            };
            //Act
            var response = service.AddDirector(director);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddDirector_validData_validResult()
        {
            //Arrange
            var directorServiceMock = new Mock<IDirectorService>();
            var service = new DirectorServiceWebApi(directorServiceMock.Object);
            var director = new DirectorDto
            {
                Id = 1,
                Name = "Jan",
                Surname = "Nowak",
                BirthDate = new DateTime(2000, 10, 10)
            };
            directorServiceMock.Setup(x => x.AddDirector(director)).Returns(2);
            //Act
            var response = service.AddDirector(director);
            //Assert
            Assert.AreEqual(2, response.Id);
            Assert.AreEqual("Jan", response.Name);
            Assert.AreEqual("Nowak", response.Surname);
            Assert.AreEqual(new DateTime(2000, 10, 10), response.BirthDate);
        }
    }
}
