﻿using System;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Models;
using FilmStudio.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FilmStudio.Tests.WebApiServicesTests
{
    [TestClass]
    public class MoviesServiceWebApiTests
    {
        [TestMethod]
        public void AddMovie_null_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            //Act
            var response = service.AddMovie(null);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddMovie_movieWithoutDirector_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var movie = new MovieDto
            {
                Id = 1,
                Title = "string",
                AverageScore = 3.14,
                Description = "string",
                Director = null,
                ReleaseYear = 2001,
                Genre = GenreDto.Adventure
            };
            //Act
            var response = service.AddMovie(movie);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddMovie_validData_validResult()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var movie = new MovieDto
            {
                Id = 1,
                Title = "string",
                Description = "string",
                Director = new DirectorDto
                {
                    Id = 1,
                    Name = "Jan",
                    Surname = "Nowak",
                    BirthDate = new DateTime(1000, 10, 10)
                },
                ReleaseYear = 2001,
                Genre = GenreDto.Adventure
            };
            movieServiceMock.Setup(x => x.AddMovie(movie)).Returns(2);
            //Act
            var response = service.AddMovie(movie);
            //Assert
            Assert.AreEqual(2, response.Id);
            Assert.AreEqual("string", response.Title);
            Assert.AreEqual("Jan", response.Director.Name);
        }

        [TestMethod]
        public void AddScore_null_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            //Act
            var response = service.AddScore(null);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddScore_movieScoreWithoutUser_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var score = new MovieScoreWebApi
            {
                Id = 1,
                Movie = new MovieDto
                {
                    Id = 1,
                    Title = "string",
                    Description = "string"
                },
                Score = 5
            };
            //Act
            var response = service.AddScore(score);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddScore_movieScoreWithoutMovie_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var score = new MovieScoreWebApi
            {
                Id = 1,
                User = new UserWebApi
                {
                    Id = 1,
                    IsAdministrator = true,
                    IsLoggedIn = true,
                    Login = "string"
                },
                Score = 5
            };
            //Act
            var response = service.AddScore(score);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddScore_movieScoreWithLowScore_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var score = new MovieScoreWebApi
            {
                Id = 1,
                Movie = new MovieDto
                {
                    Id = 1,
                    Title = "string",
                    Description = "string"
                },
                User = new UserWebApi
                {
                    Id = 1,
                    IsAdministrator = true,
                    IsLoggedIn = true,
                    Login = "string"
                },
                Score = 0
            };
            //Act
            var response = service.AddScore(score);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddScore_movieScoreWithHighScore_null()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var score = new MovieScoreWebApi
            {
                Id = 1,
                Movie = new MovieDto
                {
                    Id = 1,
                    Title = "string",
                    Description = "string"
                },
                User = new UserWebApi
                {
                    Id = 1,
                    IsAdministrator = true,
                    IsLoggedIn = true,
                    Login = "string"
                },
                Score = 11
            };
            //Act
            var response = service.AddScore(score);
            //Assert
            Assert.IsNull(response);
        }

        [TestMethod]
        public void AddScore_validData_validResult()
        {
            //Arrange
            var movieServiceMock = new Mock<IMoviesService>();
            var service = new MoviesServiceWebApi(movieServiceMock.Object);
            var score = new MovieScoreWebApi
            {
                Id = 1,
                Movie = new MovieDto
                {
                    Id = 1,
                    Title = "string",
                    Description = "string"
                },
                User = new UserWebApi
                {
                    Id = 1,
                    IsAdministrator = true,
                    IsLoggedIn = true,
                    Login = "string"
                },
                Score = 5
            };
            movieServiceMock.Setup(x => x.AddScore(It.IsAny<MovieScoreDto>())).Returns(2);
            //Act
            var response = service.AddScore(score);
            //Assert
            Assert.AreEqual(2, response.Id);
            Assert.AreEqual(1, response.Movie.Id);
            Assert.AreEqual(1, response.User.Id);
            Assert.AreEqual(5, response.Score);
        }
    }
}
