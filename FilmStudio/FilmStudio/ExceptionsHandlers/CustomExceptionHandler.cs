﻿using System.Web.Http.ExceptionHandling;


namespace FilmStudio.ExceptionsHandlers
{
    public class CustomExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            context.Result = new ErrorResult
            {
                Request = context.ExceptionContext.Request,
                Content = "Something bad happend, Call the Godfather" 
            };
        }
    }
}
