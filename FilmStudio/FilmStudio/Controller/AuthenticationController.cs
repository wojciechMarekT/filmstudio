﻿using System.Web.Http;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Controller
{
    public class AuthenticationController : ApiController
    {
        private readonly IUserServiceWebApi _userServiceWebApi;

        public AuthenticationController(IUserServiceWebApi userServiceWebApi)
        {
            _userServiceWebApi = userServiceWebApi;
        }

        [HttpPost]
        public UserWebApi Authenticate(Credentials credentials)
        {
            return _userServiceWebApi.AutheticateUser(credentials);
        }
    }
}
