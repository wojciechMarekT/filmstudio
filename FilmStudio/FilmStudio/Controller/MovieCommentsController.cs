﻿using System.Collections.Generic;
using System.Web.Http;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Controller
{
    public class MovieCommentsController : ApiController
    {
        private readonly IMoviesServiceWebApi _moviesServiceWebApi;

        public MovieCommentsController(IMoviesServiceWebApi moviesServiceWebApi)
        {
            _moviesServiceWebApi = moviesServiceWebApi;
        }

        [HttpPost]
        public CommentWebApi AddComment(CommentWebApi commentWebApi)
        {
            return _moviesServiceWebApi.AddComment(commentWebApi);
        }
        
        [HttpGet]
        public List<CommentWebApi> GetComments(int id)
        {
            return _moviesServiceWebApi.GetComments(id);
        }
    }
}
