﻿using System.Collections.Generic;
using System.Web.Http;
using FilmStudio.BuissnesLayer.Extensions;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Controller
{
    public class MoviesController : ApiController
    {
        private readonly IMoviesServiceWebApi _moviesServiceWebApi;
        private readonly IMoviesService _moviesService;
        private readonly IEnumExtension _enumExtension;

        public MoviesController(IMoviesServiceWebApi moviesServiceWebApi, IEnumExtension enumExtension,IMoviesService moviesService)
        {
            _moviesServiceWebApi = moviesServiceWebApi;
            _moviesService = moviesService;
            _enumExtension = enumExtension;
        }

        [Route("api/movies/add")]
        [HttpPost]
        public MovieDto AddMovie(MovieDto movie)
        {
            return _moviesServiceWebApi.AddMovie(movie);
        }

        [HttpGet]
        public List<MovieDto> GetAllMovies()
        {
            return _moviesServiceWebApi.GetAllMovies();
        }

        [HttpGet]
        public MovieDto GetMovieById(int id)
        {
            return _moviesService.GetMovieById(id);
        }

        [Route("api/movies/genre")]
        [HttpGet]
        public IEnumerable<EnumValue> GetGenreTypes()
        {
            return _enumExtension.GetValues<GenreDto>();
        }

        [HttpGet]
        public MoviesPage GetMoviesPage(int pageSize, int pageNo, string orderBy = "id")
        {
            return _moviesServiceWebApi.GetMoviesPage(pageSize, pageNo, orderBy);
        }

    }
}
