﻿using System.Collections.Generic;
using System.Web.Http;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Controller
{
    public class MovieScoresController : ApiController
    {
        private readonly IMoviesServiceWebApi _moviesServiceWebApi;

        public MovieScoresController(IMoviesServiceWebApi moviesServiceWebApi)
        {
            _moviesServiceWebApi = moviesServiceWebApi;
        }

        [HttpPost]
        public MovieScoreWebApi AddRate(MovieScoreWebApi movieScoreWebApi)
        {
            return _moviesServiceWebApi.AddScore(movieScoreWebApi);
        }
        
        [HttpGet]
        public List<MovieScoreWebApi> GetScores([FromUri]int movieId, [FromUri]int userId)
        {
            return _moviesServiceWebApi.GetScores(movieId, userId);
        }
    }
}
