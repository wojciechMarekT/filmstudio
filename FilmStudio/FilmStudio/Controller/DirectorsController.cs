﻿using System.Collections.Generic;
using System.Web.Http;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Controller
{
    public class DirectorsController : ApiController
    {
        private readonly IDirectorServiceWebApi _directorServiceWebApi;
        private readonly IDirectorService _directorService;

        public DirectorsController(IDirectorServiceWebApi directorServiceWebApi, IDirectorService directorService)
        {
            _directorServiceWebApi = directorServiceWebApi;
            _directorService = directorService;
        }

        [HttpPost]
        public DirectorDto AddDirector(DirectorDto directorDto)
        {
            return _directorServiceWebApi.AddDirector(directorDto);
        }

        [HttpGet]
        public List<DirectorDto> GetDirectors()
        {
            return _directorService.GetAllDirectors();
        }
    }
}
