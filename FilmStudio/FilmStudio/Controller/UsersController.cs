﻿using System.Collections.Generic;
using System.Web.Http;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Controller
{
    public class UsersController : ApiController
    {
        private readonly IUserServiceWebApi _userServiceWebApi;
        private readonly IUserService _userService;

        public UsersController(IUserServiceWebApi userServiceWebApi, IUserService userService)
        {
            _userServiceWebApi = userServiceWebApi;
            _userService = userService;
        }

        [Route("api/users/register")]
        [HttpPost]
        public UserWebApi RegisterUser(Credentials credentials)
        {
           return _userServiceWebApi.RegisterUser(credentials);
        }

        [Route("api/users/{id}/setadmin")]
        [HttpPut]
        public void SetAdminPrivilidges(int id)
        {
            _userService.SetAdminPrivilidges(id);
        }

        [Route("api/users/observe")]
        [HttpPut]
        public void ObserveUser([FromUri]int observing, [FromUri] int observed)
        {
            _userService.ObserveUSer(observing, observed);
        }

        [HttpGet]
        public List<UserWebApi> Get()
        {
            return _userServiceWebApi.GetAllUsers();
        }

        [Route("api/users/observed")]
        [HttpGet]
        public List<UserWebApi> GetObservedUsers([FromUri]int userId)
        {
           return _userServiceWebApi.GetObservedUsers(userId);
        }
    }
}
