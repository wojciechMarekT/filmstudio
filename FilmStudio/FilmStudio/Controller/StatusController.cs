﻿using System;
using System.Web.Http;

namespace FilmStudio.Controller
{
    public class StatusController : ApiController
    {
        public string Get()
        {
            return "Ok";
        }
    }
}
