﻿using System;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using FilmStudio.App_Start;
using FilmStudio.ExceptionsHandlers;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace FilmStudio
{
    internal class StartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            SwaggerConfig.Register(config);

            config.Services.Replace(typeof(IExceptionHandler), new CustomExceptionHandler());
            
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.EnableCors(new EnableCorsAttribute("*", "*", "*", "X-Custom-Header"));
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "FilmStudioApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            appBuilder.UseNinjectMiddleware(NinjectBootstrap.GetKernel).UseNinjectWebApi(config);
        }
    }
}
