﻿using FilmStudio.App_Start.Topshelf;

namespace FilmStudio
{
    internal class Program
    {
        private static void Main()
        {
            ConfigureService.Configure();
        }
    }
}
