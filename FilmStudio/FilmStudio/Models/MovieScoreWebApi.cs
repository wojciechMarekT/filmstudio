﻿using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.Models
{
    public class MovieScoreWebApi
    {
        public int Id { get; set; }
        public MovieDto Movie { get; set; }
        public UserWebApi User { get; set; }
        public int Score { get; set; }
    }
}
