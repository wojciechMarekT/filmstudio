﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.Models
{
    public class MoviesPage
    {
        public List<MovieDto> Movies { get; set; }
        public string PreviousPage { get; set; }
        public string NextPage { get; set; }
        public int NumberOfPages { get; set; }
    }
}
