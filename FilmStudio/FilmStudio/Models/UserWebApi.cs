﻿namespace FilmStudio.Models
{
    public class UserWebApi
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsAdministrator { get; set; }
    }
}
