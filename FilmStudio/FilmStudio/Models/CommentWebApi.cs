﻿using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.Models
{
    public class CommentWebApi
    {
        public int Id { get; set; }
        public UserWebApi User { get; set; }
        public MovieDto Movie { get; set; }
        public string UserComment { get; set; }
    }
}
