﻿using FilmStudio.BuissnesLayer.Models;

namespace FilmStudio.Services.Interfaces
{
    public interface IDirectorServiceWebApi
    {
        DirectorDto AddDirector(DirectorDto directorDto);
    }
}