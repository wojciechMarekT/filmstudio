﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.Models;

namespace FilmStudio.Services.Interfaces
{
    public interface IMoviesServiceWebApi
    {
        MovieDto AddMovie(MovieDto movie);
        List<MovieDto> GetAllMovies();
        MovieScoreWebApi AddScore(MovieScoreWebApi movieScoreWebApiDto);
        List<MovieScoreWebApi> GetScores(int movieId, int userId);
        CommentWebApi AddComment(CommentWebApi commentDto);
        List<CommentWebApi> GetComments(int movieId);
        MoviesPage GetMoviesPage(int pageSize, int pageNo, string orderBy);
    }
}