﻿using System.Collections.Generic;
using FilmStudio.Models;

namespace FilmStudio.Services.Interfaces
{
    public interface IUserServiceWebApi
    {
        UserWebApi RegisterUser(Credentials credentials);
        UserWebApi AutheticateUser(Credentials credentials);
        List<UserWebApi> GetAllUsers();
        List<UserWebApi> GetObservedUsers(int userId);
    }
}