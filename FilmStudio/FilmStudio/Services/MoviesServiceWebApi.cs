﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Services
{
    public class MoviesServiceWebApi : IMoviesServiceWebApi
    {
        private readonly IMoviesService _moviesService;

        public MoviesServiceWebApi(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        public List<MovieDto> GetAllMovies()
        {
            return _moviesService.GetAllMovies();
        }

        public MovieDto AddMovie(MovieDto movie)
        {
            if (movie == null || movie.Director == null)
            {
				return null;
			}

            var newId = _moviesService.AddMovie(movie);
            if (newId == 0)
            {
               return null;
            }
        
   			movie.Id = newId;
            return movie;
        }
        public MovieScoreWebApi AddScore(MovieScoreWebApi movieScoreWebApi)
        {
            if (movieScoreWebApi == null || movieScoreWebApi.User == null || movieScoreWebApi.Movie == null || movieScoreWebApi.Score < 1 ||
                movieScoreWebApi.Score > 10)
            {
                return null;
            }
            var movieScoreDto = new MovieScoreDto
            {
                User = new UserDto
                {
                    Id = movieScoreWebApi.User.Id,
                    IsAdmin = movieScoreWebApi.User.IsAdministrator,
                    Login = movieScoreWebApi.User.Login,
                },
                Movie = movieScoreWebApi.Movie,
                Id = movieScoreWebApi.Id,
                Score = movieScoreWebApi.Score
            };

            var newId = _moviesService.AddScore(movieScoreDto);
            movieScoreWebApi.Id = newId;
            return movieScoreWebApi;
        }

        public List<MovieScoreWebApi> GetScores(int movieId, int userId)
        {
            var movieScoresList = new List<MovieScoreWebApi>();
            var movieScoresDto = _moviesService.GetScores(movieId, userId);

            if (movieScoresDto != null && movieScoresDto.Count != 0)
            {
                movieScoresDto.ForEach(x => movieScoresList.Add(new MovieScoreWebApi
                {
                    User = new UserWebApi
                    {
                        Id = x.User.Id,
                        Login = x.User.Login,
                        IsAdministrator = x.User.IsAdmin,
                        IsLoggedIn = false
                    },
                    Movie = x.Movie,
                    Id = x.Id,
                    Score = x.Score
                }));
            }
            return movieScoresList;
        }

        public CommentWebApi AddComment(CommentWebApi commentWebApi)
        {
            if (commentWebApi.User == null || commentWebApi.Movie == null || string.IsNullOrWhiteSpace(commentWebApi.UserComment))
            {
                return null;
            }

            var commentDto = new CommentDto
            {
                User = new UserDto
                {
                    Id = commentWebApi.User.Id,
                    Login = commentWebApi.User.Login,
                    IsAdmin = commentWebApi.User.IsAdministrator,
                    Password = null,
                    ObservedUsers = null
                },
                Id = commentWebApi.Id,
                Movie = commentWebApi.Movie,
                UserComment = commentWebApi.UserComment
            };

            var newId = _moviesService.AddComment(commentDto);
            commentWebApi.Id = newId;

            return commentWebApi;
        }

        public List<CommentWebApi> GetComments(int movieId)
        {
            var commentsWebApi = new List<CommentWebApi>();
            var commentsDto = _moviesService.GetComments(movieId);

            if (commentsDto != null && commentsDto.Count != 0)
            {
                commentsDto.ForEach(x => commentsWebApi.Add(new CommentWebApi
                {
                    User = new UserWebApi
                    {
                        Id = x.User.Id,
                        Login = x.User.Login,
                        IsAdministrator = x.User.IsAdmin,
                        IsLoggedIn = false
                    },
                    Movie = x.Movie,
                    Id = x.Id,
                    UserComment = x.UserComment
                }));
                return commentsWebApi;
            }
            return null;
        }

        MoviesPage IMoviesServiceWebApi.GetMoviesPage(int pageSize, int pageNo, string orderBy)
        {
            var pageResultDto = _moviesService.GetPagedMovies(pageSize, pageNo, orderBy);

            if (pageNo > pageResultDto.Pages)
            {
                return null;
            }
            var moviesPage = new MoviesPage
            {
                Movies = pageResultDto.Collection,
                PreviousPage = pageNo == 1 ? null : $"movies?pageSize={pageSize}&pageNo={pageNo -1}&{orderBy}",
                NextPage = pageNo == pageResultDto.Pages ? null : $"movies?pageSize={pageSize}&pageNo={pageNo + 1}&{orderBy}",
                NumberOfPages = pageResultDto.Pages
            };
            return moviesPage;
        }
    }
}
