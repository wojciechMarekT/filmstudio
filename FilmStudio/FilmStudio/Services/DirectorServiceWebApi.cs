﻿using System;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Services
{
    public class DirectorServiceWebApi : IDirectorServiceWebApi
    {
        private readonly IDirectorService _directorService;

        public DirectorServiceWebApi(IDirectorService directorService)
        {
            _directorService = directorService;
        }

        public DirectorDto AddDirector(DirectorDto directorDto)
        {
            if (directorDto == null || string.IsNullOrWhiteSpace(directorDto.Name) || string.IsNullOrWhiteSpace(directorDto.Surname) || directorDto.BirthDate < new DateTime(1753, 1, 1))
            {
                return null;
            }

            var newId = _directorService.AddDirector(directorDto);
            if (newId == 0)
            {
                return null;
            }
            directorDto.Id = newId;
            return directorDto;
        }
    }
}
