﻿using System.Collections.Generic;
using FilmStudio.BuissnesLayer.Models;
using FilmStudio.BuissnesLayer.Services.Interfaces;
using FilmStudio.Models;
using FilmStudio.Services.Interfaces;

namespace FilmStudio.Services
{
    public class UserServiceWebApi : IUserServiceWebApi
    {
        private readonly IUserService _userService;

        public UserServiceWebApi(IUserService userService)
        {
            _userService = userService;
        }

        public UserWebApi RegisterUser(Credentials credentials)
        {
            if (credentials == null || string.IsNullOrEmpty(credentials.Password) ||
                string.IsNullOrEmpty(credentials.Username))
            {
                return null;
            }

            var userDto = new UserDto
            {
                Id = 0,
                IsAdmin = false,
                ObservedUsers = null,
                Login = credentials.Username,
                Password = credentials.Password
            };

            var resultUser = _userService.RegisterUSer(userDto);
            if (resultUser != null)
            {
                return new UserWebApi
                {
                    Id = resultUser.Id,
                    Login = resultUser.Login,
                    IsAdministrator = resultUser.IsAdmin,
                    IsLoggedIn = true
                };
            }
            return null;
        }

        public UserWebApi AutheticateUser(Credentials credentials)
        {
            var exisitngUser = _userService.GetUserByLogin(credentials.Username);

            var user = new UserWebApi
            {
                Login = credentials.Username,
                Id = 0,
                IsAdministrator = false,
                IsLoggedIn = false
            };

            if (exisitngUser == null || exisitngUser.Password != credentials.Password) return user;

            user.Id = exisitngUser.Id;
            user.IsAdministrator = exisitngUser.IsAdmin;
            user.IsLoggedIn = true;
            return user;
        }

        public List<UserWebApi> GetAllUsers()
        {
            var usersList = new List<UserWebApi>();
            var usersDto = _userService.GetAllUsers();

            usersDto.ForEach(u =>
            {
                usersList.Add(new UserWebApi
                {
                    Id = u.Id,
                    Login = u.Login,
                    IsAdministrator = u.IsAdmin,
                    IsLoggedIn = false
                });
            });

            return usersList;
        }

        public List<UserWebApi> GetObservedUsers(int userId)
        {
            var observedUsersList = new List<UserWebApi>();
            var observedUsersDto = _userService.GetObservedUsers(userId);

            observedUsersDto.ForEach(u =>
            {
                observedUsersList.Add(new UserWebApi
                {
                    Id = u.Id,
                    Login = u.Login,
                    IsAdministrator = u.IsAdmin,
                    IsLoggedIn = false
                });
            });

            return observedUsersList;
        }
    }
}
