﻿using System;
using System.Configuration;
using System.Net.Http;
using Microsoft.Owin.Hosting;

namespace FilmStudio.App_Start.Topshelf
{
    public class TopShelfService
    {
        private IDisposable _webApi;
        public void Start()
        {
            var apiAdress = ConfigurationManager.AppSettings["ApiAdress"];

            _webApi = WebApp.Start<StartUp>(apiAdress);
            
                var client = new HttpClient();
                var response = client.GetAsync(apiAdress + "api/status").Result;

                Console.WriteLine("StatusCode:" + response.StatusCode + " || " + response.RequestMessage);
             //   Console.ReadLine();
            
        }
        public void Stop()
        {
            _webApi.Dispose();
        }
    }
}
