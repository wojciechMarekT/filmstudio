﻿using Topshelf;

namespace FilmStudio.App_Start.Topshelf
{
    internal static class ConfigureService
    {
        internal static void Configure()
        {
            HostFactory.Run(configure =>
            {
                configure.Service<TopShelfService>(service =>
                {
                    service.ConstructUsing(s => new TopShelfService());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });


                configure.RunAsLocalSystem();
                configure.SetServiceName("FilmStudioWebApi");
                configure.SetDisplayName("FilmStudioWebApi");
                configure.SetDescription("WebApi for Film studio app!");
            });
        }
    }
}
