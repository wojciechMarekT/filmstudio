﻿using FilmStudio.BuissnesLayer.InjectComponents;
using FilmStudio.Services;
using FilmStudio.Services.Interfaces;
using Ninject;

namespace FilmStudio.App_Start
{
    public static class NinjectBootstrap
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel(new BusinessModule());
            kernel.Bind<IUserServiceWebApi>().To<UserServiceWebApi>();
            kernel.Bind<IDirectorServiceWebApi>().To<DirectorServiceWebApi>();
            kernel.Bind<IMoviesServiceWebApi>().To<MoviesServiceWebApi>();
            return kernel;
        }
    }
}
