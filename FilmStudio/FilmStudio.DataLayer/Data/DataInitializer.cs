﻿using System.Data.Entity;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Data
{
    internal class DataInitializer<T> : CreateDatabaseIfNotExists<FilmStudioDbContext>
    {
        protected override void Seed(FilmStudioDbContext context)
        {
            var user = new User
            {
                Id = 0,
                Login = "Admin",
                Password = "admin",
                IsAdmin = true,
                ObservingUsers = null,
                ObservedUsers = null
            };

            context.Users.Add(user);
            base.Seed(context);
        }
    }
}
