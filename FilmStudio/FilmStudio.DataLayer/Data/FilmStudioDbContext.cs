﻿using System.Configuration;
using System.Data.Entity;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Data
{
    internal class FilmStudioDbContext : DbContext
    {
        public FilmStudioDbContext() : base(GetConnectionstring())
        {
            Database.SetInitializer(new DataInitializer<FilmStudioDbContext>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<MovieScore> MovieScores { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(c => c.ObservingUsers)
                .WithMany(c => c.ObservedUsers)
                .Map(t =>
                    {
                        t.MapLeftKey("ObservedUserId");
                        t.MapRightKey("ObservingUserId");
                        t.ToTable("ObservedUsers");
                    }
               );
        }

        private static string GetConnectionstring()
        {
            return ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString;
        }
    }
}
