﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FilmStudio.DataLayer.Data;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.DataLayer.Repositories
{
    public class MovieScoreRepository : IMovieScoreRepository
    {
        public MovieScore AddScore(MovieScore score)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var existingRate = dbContext.MovieScores.SingleOrDefault(x => x.User.Id == score.User.Id && x.Movie.Id == score.Movie.Id);
                if (existingRate != null)
                {
                    return null;
                }

                var user = dbContext.Users.SingleOrDefault(x => x.Id == score.User.Id);
                score.User = user;

                var movie = dbContext.Movies.SingleOrDefault(x => x.Id == score.Movie.Id);
                score.Movie = movie;

                var addedRate = dbContext.MovieScores.Add(score);
                dbContext.SaveChanges();

                return addedRate;
            }
        }

        public List<MovieScore> GetScore(int movieId, int userId)
        {

            var movieScores = new List<MovieScore>();
            using (var dbContext = new FilmStudioDbContext())
            {
                var user = dbContext.Users
                    .Where(x => x.Id == userId)
                    .Include(x => x.ObservedUsers)
                    .SingleOrDefault();

                if (user == null) return movieScores;
                movieScores.AddRange(user.ObservedUsers
                    .Select(observed => dbContext.MovieScores
                                                 .Include(x => x.Movie)
                                                 .Where(x => x.Movie.Id == movieId)
                                                 .SingleOrDefault(x => x.User.Id == observed.Id))
                    .Where(score => score != null));

               var userScore = dbContext.MovieScores
                                        .Include(x => x.Movie)
                                        .Where(x => x.Movie.Id == movieId && x != null)
                                        .SingleOrDefault(x => x.User.Id == userId);
                if (userScore != null)
                {
                    movieScores.Add(userScore);
                }
                return movieScores;
            }
        }
    }
}
