﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FilmStudio.DataLayer.Data;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.DataLayer.Repositories
{
    public class MovieCommentRepository : IMovieCommentRepository
    {
        public Comment AddComment(Comment comment)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(x => x.Id == comment.User.Id);
                comment.User = user;

                var movie = dbContext.Movies.SingleOrDefault(x => x.Id == comment.Movie.Id);
                comment.Movie = movie;

                var addedComment = dbContext.Comments.Add(comment);
                dbContext.SaveChanges();

                return addedComment;
            }
        }

        public List<Comment> GetComments(int movieId)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var comments = dbContext.Comments.Where(x => x.Movie.Id == movieId).Include(x => x.Movie).Include(x => x.User).ToList();
                return comments;
            }
        }
    }
}
