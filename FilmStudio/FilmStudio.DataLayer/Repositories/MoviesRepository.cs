﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using FilmStudio.DataLayer.Data;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.DataLayer.Repositories
{
    public class MoviesRepository : IMoviesRepository
    {
        public int AddMovie(Movie movie)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var existingMovie = dbContext.Movies
                    .SingleOrDefault(m => m.Title == movie.Title && m.Director.Id == movie.Director.Id);

                if (existingMovie != null)
                {
                    return 0;
                }
                var director = dbContext.Directors.SingleOrDefault(d => d.Id == movie.Director.Id);
                movie.Director = director;

                var newMovie = dbContext.Movies.Add(movie);
                dbContext.SaveChanges();

                return newMovie.Id;
            }
        }

        public List<Movie> GetAllMovies()
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var movies = dbContext.Movies.Include(x => x.Director)
                    .ToList();

                return movies;
            }
        }

        public Movie GetMovieById(int movieId)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var movie = dbContext.Movies.Include(x => x.Director).SingleOrDefault(m => m.Id == movieId);

                return movie;
            }
        }

        public double GetMovieScore(int movieId)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var isScoreExists = dbContext.MovieScores.Include(m => m.Movie)
                    .Any(s => s.Movie.Id == movieId);

                if (!isScoreExists)
                {
                    return 0.0;
                }
                var score = dbContext.MovieScores.Include(m => m.Movie)
                    .Where(s => s.Movie.Id == movieId)
                    .Average(s => (double)s.Score);

                return score;
            }
        }

        public PageResults<Movie> GetPagedMovies(int pageSize, int pageNo, string orderBy)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var prop = TypeDescriptor.GetProperties(typeof(Movie)).Find(orderBy,true);

                var query = dbContext.Movies.Include(x => x.Director);
                  //  .OrderBy(x => prop.GetValue(x)); 

                var pageMovies = query.ToList().Skip(pageSize * (pageNo - 1)).Take(pageSize).ToList();
                var entitiesCount = dbContext.Movies.Count();

                var page = new PageResults<Movie>
                {
                    Collection = pageMovies,
                    Pages = (int)Math.Ceiling(entitiesCount/(double)pageSize)
                };
               return page;
            }
        }
    }
}

