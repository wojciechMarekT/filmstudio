﻿using System.Collections.Generic;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Repositories.Interfaces
{
    public interface IDirectorRepository
    {
        int AddDirector(Director director);
        List<Director> GetDiretors();
    }
}