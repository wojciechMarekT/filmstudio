﻿using System.Collections.Generic;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Repositories.Interfaces
{
    public interface IMovieCommentRepository
    {
        Comment AddComment(Comment comment);
        List<Comment> GetComments(int movieId);
    }
}