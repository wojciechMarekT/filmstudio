﻿using System.Collections.Generic;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Repositories.Interfaces
{
    public interface IMovieScoreRepository
    {
        MovieScore AddScore(MovieScore score);
        List<MovieScore> GetScore(int movieId, int userId);
    }
}