﻿using System.Collections.Generic;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Repositories.Interfaces
{
    public interface IUserRepository
    {
        User GetUserById(int userId);
        User GetUserByLogin(string login);
        User RegisterUser(User user);
        void SetAdministratorPriviliges(int userId);
        List<User> GetAllUsers();
        void ObserveUser(int observing, int observed);
        List<User> GetObservedUsers(int userId);
    }
}