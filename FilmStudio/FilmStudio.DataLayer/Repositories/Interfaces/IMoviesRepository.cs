﻿using System.Collections.Generic;
using FilmStudio.DataLayer.Models;

namespace FilmStudio.DataLayer.Repositories.Interfaces
{
    public interface IMoviesRepository
    {
        int AddMovie(Movie movie);
        List<Movie> GetAllMovies();
        Movie GetMovieById(int movieId);
        double GetMovieScore(int movieId);
        PageResults<Movie> GetPagedMovies(int pageSize, int pageNo, string orderBy);
    }
}