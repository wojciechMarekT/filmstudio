﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using FilmStudio.DataLayer.Data;
using FilmStudio.DataLayer.Models;
using System.Linq;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.DataLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        public User RegisterUser(User user)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var existingUser = dbContext.Users.SingleOrDefault(x => x.Login == user.Login);
                if (existingUser != null)
                {
                    return null;
                }

                var registeredUser = dbContext.Users.Add(user);
                dbContext.SaveChanges();

                return registeredUser;
            }
        }

        public User GetUserByLogin(string login)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(x => x.Login == login);
                return user;
            }
        }

        public User GetUserById(int userId)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(x => x.Id == userId);
                return user;
            }
        }

        public void SetAdministratorPriviliges(int userId)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(x => x.Id == userId);
                if (user == null)
                {
                    return;
                }

                user.IsAdmin = true;
                dbContext.Users.Attach(user);
                dbContext.Entry(user).Property(x => x.IsAdmin).IsModified = true;
                dbContext.SaveChanges();
            }
        }

        public void ObserveUser(int observing, int observed)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var observingUser = dbContext.Users
                    .Where(u => u.Id == observing)
                    .Include(x => x.ObservedUsers)
                    .SingleOrDefault();
                var observedUser = dbContext.Users.SingleOrDefault(u => u.Id == observed);

                if (observedUser == null || observingUser == null)
                {
                    return;
                }

                observingUser.ObservedUsers.Add(observedUser);
                dbContext.Users.Attach(observingUser);
              //  dbContext.Entry(observingUser).Property(x => x.ObservedUsers).IsModified = true;
                dbContext.SaveChanges();
            }
        }

        public List<User> GetAllUsers()
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var users = dbContext.Users.ToList();
                return users;
            }
        }

        public List<User> GetObservedUsers(int userId)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var observingUser = dbContext.Users
                    .Where(u => u.Id == userId)
                    .Include(x => x.ObservedUsers)
                    .SingleOrDefault();

                return observingUser?.ObservedUsers;
            }
        }
    }
}
