﻿using System.Collections.Generic;
using System.Linq;
using FilmStudio.DataLayer.Data;
using FilmStudio.DataLayer.Models;
using FilmStudio.DataLayer.Repositories.Interfaces;

namespace FilmStudio.DataLayer.Repositories
{
    public class DirectorRepository : IDirectorRepository
    {
        public int AddDirector(Director director)
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var existingDirector = dbContext.Directors.SingleOrDefault(x => x.Name == director.Name && x.Surname == director.Surname && x.BirthDate == director.BirthDate);
                if (existingDirector != null)
                {
                    return 0;
                }

                var addedDirector = dbContext.Directors.Add(director);
                dbContext.SaveChanges();

                return addedDirector.Id;
            }
        }

        public List<Director> GetDiretors()
        {
            using (var dbContext = new FilmStudioDbContext())
            {
                var directors = dbContext.Directors
                    .ToList();

                return directors;
            }
        }
    }
}
