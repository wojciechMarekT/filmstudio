﻿using FilmStudio.DataLayer.Repositories;
using FilmStudio.DataLayer.Repositories.Interfaces;
using Ninject.Modules;

namespace FilmStudio.DataLayer.InjectComponents
{
    public class DataModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserRepository>().To<UserRepository>();  
            Bind<IDirectorRepository>().To<DirectorRepository>();
            Bind<IMoviesRepository>().To<MoviesRepository>();
            Bind<IMovieScoreRepository>().To<MovieScoreRepository>();
            Bind<IMovieCommentRepository>().To<MovieCommentRepository>();
        }
    }
}
