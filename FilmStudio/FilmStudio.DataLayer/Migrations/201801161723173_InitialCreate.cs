namespace FilmStudio.DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserComment = c.String(),
                        Movie_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Movies", t => t.Movie_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Movie_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Movies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ReleaseYear = c.Int(nullable: false),
                        Description = c.String(),
                        Genre = c.Int(nullable: false),
                        Director_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Directors", t => t.Director_Id)
                .Index(t => t.Director_Id);
            
            CreateTable(
                "dbo.Directors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.String(),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MovieScores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Score = c.Int(nullable: false),
                        Movie_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Movies", t => t.Movie_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Movie_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ObservedUsers",
                c => new
                    {
                        ObservingUserId = c.Int(nullable: false),
                        ObservedUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ObservingUserId, t.ObservedUserId })
                .ForeignKey("dbo.Users", t => t.ObservingUserId)
                .ForeignKey("dbo.Users", t => t.ObservedUserId)
                .Index(t => t.ObservingUserId)
                .Index(t => t.ObservedUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MovieScores", "User_Id", "dbo.Users");
            DropForeignKey("dbo.MovieScores", "Movie_Id", "dbo.Movies");
            DropForeignKey("dbo.Comments", "User_Id", "dbo.Users");
            DropForeignKey("dbo.ObservedUsers", "ObservedUserId", "dbo.Users");
            DropForeignKey("dbo.ObservedUsers", "ObservingUserId", "dbo.Users");
            DropForeignKey("dbo.Comments", "Movie_Id", "dbo.Movies");
            DropForeignKey("dbo.Movies", "Director_Id", "dbo.Directors");
            DropIndex("dbo.ObservedUsers", new[] { "ObservedUserId" });
            DropIndex("dbo.ObservedUsers", new[] { "ObservingUserId" });
            DropIndex("dbo.MovieScores", new[] { "User_Id" });
            DropIndex("dbo.MovieScores", new[] { "Movie_Id" });
            DropIndex("dbo.Movies", new[] { "Director_Id" });
            DropIndex("dbo.Comments", new[] { "User_Id" });
            DropIndex("dbo.Comments", new[] { "Movie_Id" });
            DropTable("dbo.ObservedUsers");
            DropTable("dbo.MovieScores");
            DropTable("dbo.Users");
            DropTable("dbo.Directors");
            DropTable("dbo.Movies");
            DropTable("dbo.Comments");
        }
    }
}
