namespace FilmStudio.DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SwitchObservingUsersKeys : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ObservedUsers", name: "ObservingUserId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.ObservedUsers", name: "ObservedUserId", newName: "ObservingUserId");
            RenameColumn(table: "dbo.ObservedUsers", name: "__mig_tmp__0", newName: "ObservedUserId");
            RenameIndex(table: "dbo.ObservedUsers", name: "IX_ObservingUserId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.ObservedUsers", name: "IX_ObservedUserId", newName: "IX_ObservingUserId");
            RenameIndex(table: "dbo.ObservedUsers", name: "__mig_tmp__0", newName: "IX_ObservedUserId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ObservedUsers", name: "IX_ObservedUserId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.ObservedUsers", name: "IX_ObservingUserId", newName: "IX_ObservedUserId");
            RenameIndex(table: "dbo.ObservedUsers", name: "__mig_tmp__0", newName: "IX_ObservingUserId");
            RenameColumn(table: "dbo.ObservedUsers", name: "ObservedUserId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.ObservedUsers", name: "ObservingUserId", newName: "ObservedUserId");
            RenameColumn(table: "dbo.ObservedUsers", name: "__mig_tmp__0", newName: "ObservingUserId");
        }
    }
}
