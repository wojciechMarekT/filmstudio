﻿using System.Collections.Generic;

namespace FilmStudio.DataLayer.Models
{
    public class PageResults<T>
    {
        public List<T> Collection { get; set; }
        public int Pages { get; set; }
    }

}
