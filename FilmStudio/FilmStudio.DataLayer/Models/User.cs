﻿using System.Collections.Generic;

namespace FilmStudio.DataLayer.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public List<User> ObservedUsers { get; set; }
        public List<User> ObservingUsers { get; set; }
    }
}
