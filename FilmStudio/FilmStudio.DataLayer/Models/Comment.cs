﻿namespace FilmStudio.DataLayer.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Movie Movie { get; set; }
        public string UserComment { get; set; }
    }
}
