﻿namespace FilmStudio.DataLayer.Models
{
    public enum Genre { Thriller, Comedy, Horror, Action, SciFi, Adventure, Drama }

    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Director Director { get; set; }
        public int ReleaseYear { get; set; }
        public string Description { get; set; }
        public Genre Genre { get; set; }
    }
}
