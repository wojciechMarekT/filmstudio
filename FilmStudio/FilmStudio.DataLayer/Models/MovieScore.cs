﻿namespace FilmStudio.DataLayer.Models
{
    public class MovieScore
    {
        public int Id { get; set; }
        public Movie Movie { get; set; }
        public User User { get; set; }
        public int Score { get; set; }
    }
}
